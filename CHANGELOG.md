# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [] - yyyy-mm-dd

### Added

### Changed

### Fixed

## [0.2.1a] - 2023-11-08

### Fixed

- Tests

## [0.2.0a] - 2023-11-04

### Added

More detailed routing options, return edges etc

### Changed

### Fixed

## [0.1.0a] - 2023-11-02

Initial Release
